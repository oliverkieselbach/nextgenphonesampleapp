//
//  ViewController.swift
//  NextgenPhoneSampleApp
//
//  Created by Kieselbach, Oliver on 01.04.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import NextGenPhoneFramework
import Accounts

class SampleIntentView: UIView {
    
    var avatar: UIView = {

        let v = UIView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 30).isActive = true
        v.widthAnchor.constraint(equalToConstant: 30).isActive = true
        v.layer.cornerRadius = 15
        v.backgroundColor = .darkGray
        
        
        return v
        
    }()
    
    var intentLabel: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        label.textAlignment = .center
        label.text = "Bitte um Rückruf"
        
        return label
    }()
    
    var attachmentButton: UIButton = {
        
        let button = UIButton(type: .custom)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 20).isActive = true
        button.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
        button.backgroundColor = .clear
        
        button.tintColor = UIView().tintColor
        button.setImage(UIImage(imageLiteralResourceName: "play").withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        
        return button
        
        
    }()

    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .lightGray
        
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.spacing = 10
        
        stackView.addArrangedSubview(self.avatar)
        stackView.addArrangedSubview(self.intentLabel)
        stackView.addArrangedSubview(self.attachmentButton)

        //stackView.setCustomSpacing(30, after: self.intentLabel)

        
        self.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        stackView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
        stackView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true

        
        self.widthAnchor.constraint(equalTo: stackView.widthAnchor, constant: 40).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

class DemoAuthenticationViewController: UIViewController {

    var signInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        self.view.backgroundColor = .white
        self.title = "Authenticate"
        
        signInButton = UIButton(frame: .zero)
        self.view.addSubview(signInButton)
        signInButton.translatesAutoresizingMaskIntoConstraints = false
        signInButton.setTitle("Sign In", for: .normal)
        signInButton.setTitle("Sign Out", for: .selected)
        signInButton.setTitleColor(.white, for: .normal)
        signInButton.backgroundColor = self.view.tintColor
        signInButton.layer.cornerRadius = 8
        signInButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        signInButton.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.5).isActive = true
        signInButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
        signInButton.addTarget(self, action: #selector(self.signIn), for: .touchDown)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.stateChange), name: Notification.Name("userSignedIn"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.stateChange), name: Notification.Name("userSignedOut"), object: nil)
        
        let v = SampleIntentView(frame: .zero)
        self.view.addSubview(v)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 50).isActive = true
        v.topAnchor.constraint(equalTo: signInButton.bottomAnchor, constant: 20).isActive = true

        v.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        v.layer.cornerRadius = 25

    }
    
    @objc func stateChange() {

        self.signInButton.isSelected = ConversationKit.shared.isSignedIn()
        
    }
    
    @objc func signIn() {
        
        if(ConversationKit.shared.isSignedIn()) {
            ConversationKit.auth.signOut()
        }
        else {
            ConversationKit.requestAuthenticationToken()
        }
        

    }


}


