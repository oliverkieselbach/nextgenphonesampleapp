//
//  DemoCKContactViewController.swift
//  NextgenPhoneSampleApp
//
//  Created by Kieselbach, Oliver on 11.04.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import NextGenPhoneFramework

class DemoCKContactViewController: UIViewController {
    
    var contactVC: CKContactViewController!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        self.title = "Single"

        
        self.contactVC = CKContactViewController(phoneNumber: "+4915157118518", width: 120)
        self.addChild(self.contactVC)
        
        self.contactVC.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.contactVC.view)
        
        self.contactVC.view.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        self.contactVC.view.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
        
        self.contactVC.delegate = self

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DemoCKContactViewController: CKContactViewDelegate {
    func didTap(contact: CKContact) {
        print(contact.name)
    }
}

