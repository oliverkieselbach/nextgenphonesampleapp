//
//  DemoCKContactCollectionViewController.swift
//  NextgenPhoneSampleApp
//
//  Created by Kieselbach, Oliver on 11.04.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import NextGenPhoneFramework

class DemoCKContactCollectionViewController: UIViewController {

    var ckContactCollectionVC: CKContactCollectionViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        self.title = "Collection"
        
        self.ckContactCollectionVC = CKContactCollectionViewController(itemSize: CGSize(width: 100, height: 200))
        self.addChild(self.ckContactCollectionVC)
        self.view.addSubview(self.ckContactCollectionVC.view)
        
        self.ckContactCollectionVC.collectionView.backgroundColor = .white
        self.ckContactCollectionVC.view.translatesAutoresizingMaskIntoConstraints = false
        self.ckContactCollectionVC.view.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
        self.ckContactCollectionVC.view.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.ckContactCollectionVC.view.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.ckContactCollectionVC.view.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        self.ckContactCollectionVC.loadData(order: CKContactOrder.statusUpdated)
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
