//
//  DemoCKContactTableViewController.swift
//  NextgenPhoneSampleApp
//
//  Created by Kieselbach, Oliver on 11.04.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import NextGenPhoneFramework

class DemoCKContactTableViewController: UIViewController {

    var ckContactTableViewVC: CKContactTableViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        self.title = "Table"
        
        self.ckContactTableViewVC = CKContactTableViewController()
        self.addChild(self.ckContactTableViewVC)
        self.view.addSubview(self.ckContactTableViewVC.view)
        
        self.ckContactTableViewVC.tableView.backgroundColor = .white
        self.ckContactTableViewVC.view.frame = self.view.frame
        
        self.ckContactTableViewVC.loadData(order: CKContactOrder.statusUpdated)

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
