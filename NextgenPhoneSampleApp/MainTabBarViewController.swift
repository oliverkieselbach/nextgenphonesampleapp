//
//  MainTabBarViewController.swift
//  NextgenPhoneSampleApp
//
//  Created by Kieselbach, Oliver on 11.04.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let vc0 = UINavigationController(rootViewController: DemoAuthenticationViewController(nibName: nil, bundle: nil))
        vc0.tabBarItem = UITabBarItem(title: "Authenticate", image: nil, tag: 0)

        let vc1 = UINavigationController(rootViewController: DemoCKContactViewController(nibName: nil, bundle: nil))
        vc1.tabBarItem = UITabBarItem(title: "Contact", image: nil, tag: 0)

        let vc2 = UINavigationController(rootViewController: DemoCKContactCollectionViewController(nibName: nil, bundle: nil))
        vc2.tabBarItem = UITabBarItem(title: "Collection", image: nil, tag: 0)

        let vc3 = UINavigationController(rootViewController: DemoCKContactTableViewController(nibName: nil, bundle: nil))
        vc3.tabBarItem = UITabBarItem(title: "Table", image: nil, tag: 0)

        self.viewControllers = [vc0, vc1, vc2, vc3]
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
